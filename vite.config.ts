import { defineConfig } from 'vite';
import dts from 'vite-plugin-dts';

export default defineConfig({
  build: {
    lib: {
      entry: 'src/main.ts',
      name: 'nodemcu',
      fileName: (format) => `nodemcu.${format}.js`
    },
    rollupOptions: {
      external: ['@johnnycubides/blockly-catalejo/core'],
      // output: {
      //   globals: {
      //     blockly: 'Blockly'
      //   }
      // }
    }
  },
  plugins: [dts({
    // outDir: 'dist/types',
    outDir: 'dist',
    insertTypesEntry: true
  })]
});
