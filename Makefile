NODE=v21.7.3
NVM=sh $$NVM_BIN

d:dev
b: build
p: publish

dependencies:
	npm update
	$(NVM) use $(NODE)

init:
	npm install

dev:
	npm run dev

build:
	npm run build

build-only:
	npm run build-only

publish: build
	npm publish --access public

clean:
	rm -rf dist
