# Nodemcu blockly catalejo

Bloques representativos de nodemcu, traduce de blockly a Lua

```bash
make init
make dev
make build
make publish
```

## Dev

```
make dev
```

## How publish in npmjs

Debe registrar el computador con su cuenta de npmjs:

```
npm adduser
```

En el `package.json` se debe asegura de tener esta configuración,
junto con las demás configuraciones.

```json
{
  "name": "@johnnycubides/nodemcu-blockly-catalejo",
  "private": false,
  "files": ["dist"]
}
```

Donde:
- **name**: Corresponde al nombre del paquete junto con el path de usuario
registrado.
- **private**: si por alguna razón o por defecto está en true, debe ser false
para que pueda ser registrado en npm.
- **files**: Allí se indica qué arhivos serán registrados en el momento de
subirlos.

A continuación se inidcan los pasos para iniciar el proceso:

```bash
git init --initial-branch=main
git remote add origin git@gitlab.com:catalejo-team/catalejo-dev/nodemcu-blockly-catalejo.git
git add .
# Los siguientes comando se pueden realizar desde gitui
git add . 
git commit -m "Initial commit"
git push --set-upstream origin main
```

## Referencias

https://docs.npmjs.com/creating-and-publishing-scoped-public-packages

Regards,

Johnny Cubides.
