#!/bin/python

# How use: python toolbox.py toolbox.xml nameToolboxVarTS export/default

import sys

xmltoolbox = "xmltoolbox.xml"
if sys.argv[1] != None:
    xmltoolbox = sys.argv[1]

nameToolboxVarTS = "toolbox"
if sys.argv[2] != None:
    nameToolboxVarTS = sys.argv[2]

def main():
    if sys.argv[3] == 'export':
        export()
    else:
        default()


def default():
    with open(xmltoolbox, "r") as fp:
        file = open("toolbox.ts", "w")
        file.write('let ' + nameToolboxVarTS + ': string = "";\n')
        for line in fp:
            line = line.replace('\'', '\\\'')
            file.write(nameToolboxVarTS + ' += '+'\''+line.replace('\n', "")+'\';\n')
        file.close()

def export():
    with open(xmltoolbox, "r") as fp:
        file = open("toolbox.ts", "w")
        file.write('export const ' + nameToolboxVarTS + ' = \n`\n')
        for line in fp:
            file.write(line)
        file.write('`')
        file.close()

main()
