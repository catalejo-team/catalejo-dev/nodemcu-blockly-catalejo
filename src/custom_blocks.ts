import { addGpioBlocks } from './gpio';
import { addActuatorsBlocks } from './actuators'
import { addModulesBlocks } from './modules'
import { addComunicationBlocks } from './comunication';
import { addHTMLBlocks } from './html';
import { addIOTBlocks } from './iot';
import { addMusicBlocks } from './music';
import { addMyFunctionsBlocks } from './myFunctions';
import { addSensorsBlocks } from './sensors';
import { addTimerBlocks } from './timers';
import { addWifiBlocks } from './wifi';

export function addCustomBlocks(Blockly: any, luaGenerator: any) {
  addGpioBlocks(Blockly, luaGenerator);
  addActuatorsBlocks(Blockly, luaGenerator);
  addModulesBlocks(Blockly, luaGenerator);
  addComunicationBlocks(Blockly, luaGenerator);
  addHTMLBlocks(Blockly, luaGenerator);
  addIOTBlocks(Blockly, luaGenerator);
  addMusicBlocks(Blockly, luaGenerator);
  addMyFunctionsBlocks(Blockly, luaGenerator);
  addSensorsBlocks(Blockly, luaGenerator);
  addTimerBlocks(Blockly, luaGenerator);
  addWifiBlocks(Blockly, luaGenerator);
}

