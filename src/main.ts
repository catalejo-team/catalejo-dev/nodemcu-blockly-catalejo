// import * as Blockly from '@johnnycubides/blockly-catalejo';
import { addCustomBlocks } from './custom_blocks';
import { toolbox } from './toolbox/toolbox'

/** Generar código desde el workspace en formato string */
function generateCode(luaGenerator: any, workspace: any): string {
  luaGenerator.INFINITE_LOOP_TRAP = null;
  return luaGenerator.workspaceToCode(workspace);
}

export {
  toolbox,
  addCustomBlocks,
  generateCode
};
